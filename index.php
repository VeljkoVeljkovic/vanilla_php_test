<?php
 class Test
{
    public static function repeat(Array $array) {
        $a = [];
       for ($i=1; $i<=3; $i++) {
        $a = array_merge($a, $array);
       }
        return $a;
    }

   

   public static  function reformat(String $str)
    {        
        $vowels = array("a", "e", "o", "E");
        return ucfirst(str_replace($vowels, "",  strtolower($str)));
    }

   public static function next_binary_number(Array $array)
    {
        $count = count($array);
       
        for ($i = $count -1; $i >= 0; $i--) {
           
            if (++$array[$i] == 2) {
                $array[$i] = 0;
            } else {
                break;
            }
        }
        
        if ($array[0] === 0) {
            return array_merge([1], $array);
        }
      
       return $array;
    }
  

}

$repeat =  Test::repeat([1,2,3]);
var_dump($repeat); 

$reformat =  Test::reformat("TyPEqaSt DeveLoper TeST");
var_dump($reformat); 

$next_binary_number =   Test::next_binary_number([1,0,0,0,0,0,0,0,0,1]);
var_dump($next_binary_number); 




